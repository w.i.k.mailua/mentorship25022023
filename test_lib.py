import pytest

import lib

test_cases_get_sum_two_numbers = [
    (0.1, 0.2, 0.3),
    (5, 6, 11),
    (0, 0, 0),
]


# @pytest.mark.parametrize(  # just to show the variant to transfer parameters
#     'number1,number2,expected',
#     [
#         (0.1, 0.2, 0.3),
#         (5, 6, 11),
#         (0, 0, 0),
#     ],
# )
# def test_get_sum_two_numbers(number1, number2, expected):
#     assert lib.get_sum_two_numbers(number1, number2) == pytest.approx(expected), 'You have got an error'

@pytest.mark.parametrize('number1,number2,expected', test_cases_get_sum_two_numbers)
def test_get_sum_two_numbers(number1, number2, expected):
    assert lib.get_sum_two_numbers(number1, number2) == pytest.approx(expected), 'You have got an error'


@pytest.mark.parametrize('number1,number2,expected', test_cases_get_sum_two_numbers)
def test_return_type_get_sum_two_numbers(number1, number2, expected):
    assert type(lib.get_sum_two_numbers(number1, number2)) in (int, float), 'You have got an error'


def test_get_sum_two_numbers_expected_error():
    with pytest.raises(ValueError):
        assert lib.get_sum_two_numbers([], 5)


def test_get_data_by_url():  # without mock data, real data
    assert lib.get_data_by_url()


def test_new_york_weather_today(mocker):
    mocker.patch('lib.get_data_by_url', return_value={'weather_today': 32})
    assert lib.get_data_for_new_york() == 0
