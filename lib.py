from typing import Union
from functools import wraps

import requests

IS_TEST = True

NEW_YORK_API = 'https://home.openweathermap.org/NEW_YORK_API'

admin = {
    'login': 'admin',
    'password': '123',
}


def get_data_by_url(url: str = 'https://dummyjson.com/products') -> dict:
    """
    if we have got status code not 2**, we just return empty dict (spec-1256)
    works only with json data
    """
    response = requests.get(url)

    if response.status_code // 100 == 2:
        result = response.json()
    else:
        result = {}

    return result


def get_data_for_new_york() -> float:
    """
    https://www.google.com/search?q=afhtyutqn+d+wtkmcbq&rlz=1C1CHZN_enUA981UA981&oq=afhtyutqn+d+wtkmcbq&
    aqs=chrome..69i57j0i10i433i512j0i10i512l8.3755j0j7&sourceid=chrome&ie=UTF-8
    (32 °F − 32) × 5/9 = 0 °C
    (2 °C × 9/5) + 32 = 35,6 °F
    """
    data = get_data_by_url(NEW_YORK_API)
    temp_far = data['weather_today']
    temp_cel = (temp_far - 32) * 5 / 9
    return temp_cel


class AuthError(Exception):
    pass


def auth(is_test):
    def inner_wrapper(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if is_test:
                result = func(*args, **kwargs)
                return result
            password = input('Enter your password -> ')
            if password == admin.get('password'):
                result = func(*args, **kwargs)
                return result
            raise AuthError

        return wrapper

    return inner_wrapper


def int_float_value_checker(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        values = [*args]
        values.extend(kwargs.values())

        for value in values:
            if not isinstance(value, (int, float)):
                raise ValueError('I have got not a number')

        result = func(*args, **kwargs)
        return result

    return wrapper


@auth(is_test=IS_TEST)
@int_float_value_checker
def get_sum_two_numbers(number1: Union[int, float], number2: Union[int, float]) -> Union[int, float]:
    summa = number1 + number2
    return summa


@int_float_value_checker
def multiply_to_two(number: Union[int, float]) -> float:
    result = number * 2.0
    return result


@int_float_value_checker
def multiply_two_arguments(number1: Union[int, float], number2: Union[int, float]) -> float:
    result = number1 * number2
    return result
