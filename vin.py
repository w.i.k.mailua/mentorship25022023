def _is_valid_vin(vin, north_american=False):
    """
    Args:
        vin(str: vehicle identification number
        north_american(bool):   Validate VIN against the 9th position checksum for north american production
            See http://en.wikipedia.org/wiki/Vehicle_Identification_Number#Check_digit_calculation

    Returns:

    """

    ILLEGAL_ALL = {'I', 'O', 'Q'}

    if len(vin) != 17:
        return _('VIN must be 17 characters')
    vin = vin.upper()

    if set(vin).intersection(ILLEGAL_ALL):
        return _('VIN cannot contain "I", "O", or "Q"')

    if north_american:
        POSITIONAL_WEIGHTS = (8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2)
        ILLEGAL_TENTH = {'U', 'Z', '0'}
        LETTER_KEY = dict(
            A=1, B=2, C=3, D=4, E=5, F=6, G=7, H=8,
            J=1, K=2, L=3, M=4, N=5, P=7, R=9,
            S=2, T=3, U=4, V=5, W=6, X=7, Y=8, Z=9,
        )

        if vin[9] in ILLEGAL_TENTH:
            return _('VIN cannot contain "U", "Z", or "0" in position 10')

        check_digit = vin[8]

        pos = sum = 0
        for char in vin:
            value = int(LETTER_KEY.get(char, char))
            weight = POSITIONAL_WEIGHTS[pos]

            sum += (value * weight)
            pos += 1

        calc_check_digit = int(sum) % 11

        if calc_check_digit == 10:
            calc_check_digit = 'X'

        if str(check_digit) != str(calc_check_digit):
            return _('Invalid VIN')

    return True
