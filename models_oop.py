from abc import ABC, abstractmethod
from datetime import date


class Creache(ABC):
    dnk_triplet5656 = 'YXY'
    blood_group_I, blood_group_II, blood_group_III, blood_group_IV = range(1, 5)

    def __init__(self, chromosom_pairs: int, type: str, name: str, additional: str = ''):
        self.name = name
        self.type = type
        self.chrom = chromosom_pairs
        self.additional = additional

    @abstractmethod
    def __str__(self):
        pass


class Cow(Creache):
    def __init__(self, chromosom_pairs: int, type: str, name: str, additional: str = '', milk_productivity: float = 0,
                 date_: date = None):
        super().__init__(chromosom_pairs=chromosom_pairs, type=type, name=name, additional=additional)
        self.milk_productivity = milk_productivity
        self.birth = date_ or date.today()

    def __str__(self):
        return self.name
