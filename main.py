import lib as main_lib

data = main_lib.get_data_by_url()
first_product = data.get('products')[0]
first_product_stock = first_product['stock']
first_product_price = first_product['price']

amount = main_lib.multiply_two_arguments(first_product_stock, first_product_price)
print(amount)
